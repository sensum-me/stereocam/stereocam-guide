#!/bin/bash

xhost +local:ROOT
docker exec -it --user "docker_vsaop" vsaop \
  /bin/bash -c "source /opt/ros/noetic/setup.bash; cd /home/docker_vsaop/catkin_ws; /bin/bash"
