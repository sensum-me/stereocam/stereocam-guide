#!/bin/bash

xhost +local:ROOT

docker exec -it --user "docker_vsaop" vsaop /bin/bash -c \
            "source /opt/ros/noetic/setup.bash;
            cd /home/docker_vsaop/catkin_ws;
            source install/setup.bash;
            roslaunch vsaop_driver demo.launch;
            /bin/bash;"
