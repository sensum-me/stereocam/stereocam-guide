#!/bin/bash

cd "$(dirname "$0")"
cd ../..
workspace_dir=$PWD

ARCH=`uname -m`

if [ "$(docker ps -aq -f status=exited -f name=vsaop)" ]; then
  docker rm vsaop
fi

image=sensum4d/sensum_stereocam:latest

docker run -it -d --rm \
  --name vsaop \
  -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
  -e DISPLAY=$DISPLAY \
  -e NVIDIA_DRIVER_CAPABILITIES=all\
  -e QT_X11_NO_MITSHM=1 \
  -e XAUTHORITY \
  -e NVIDIA_VISIBLE_DEVICES=all \
  --net "host" \
  --privileged \
  ${image}
